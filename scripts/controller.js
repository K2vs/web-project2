"use strict";
$(document).ready(() => {
  const paddle = new Paddle(200);
  const ball = new Ball(Scene_Width / 2, Scene_Height / 2, 1, 0.5);
  const brickWall = createBricks();
  let brickToRemove = [];
  const player = new Player(this.score, this.life);
  let level = 1;
  ballStartWait();
  /**
   * Start the game when the player click.
   */
  function ballStartWait() {
    displayMessage(Start_Message);
    $(document).mouseup(() => ballStart());
  }

  /**
   * Start the game.
   */
  function ballStart() {
    $(document).off("mouseup");
    hideMessage();
    $(document).mousemove(function (e) {
      const position = e.clientX;
      paddle.moveTo(position - $("#scene").offset().left - Paddle_Width / 2);
      displayPaddle(paddle);
      if (checkCollisionPaddle(ball, paddle)) {
        ball.hitThePadlle();
        console.log(ball.dx + "," + ball.dy);
      }
    });
    const interval = setInterval(() => {
      displayLevel(level);
      ball.move();
      displayBall(ball);
      brickWall.forEach((EachBrick) => {
        brickToRemove = [];
        if (checkCollisionBrick(ball, EachBrick)) {
          brickToRemove.push(EachBrick);
          brickWall.splice(brickWall.indexOf(EachBrick), 1);
          removeBrick(EachBrick);
          ball.hitTheBricks(EachBrick);
          player.addToScore(Brick_Destruction_Point);
          updateScore(player.score);
          updateLife(player._life);
        }
        if (ball.touchTheFlor()) {
          player.removeLife();
          updateLife(player._life);
          ball.placeInTheMiddle();
          displayBall(ball);
          ball._reverseY();
        }
        if (hasWin(brickWall, player)) {
          displayMessage(Win_Message);
          player.addLife();
          updateLife(player._life);
          displayBricks(createBricks());
          ball.placeInTheMiddle();
          level++;
        }
        if (hasLost(player)) {
          clearInterval(interval);
          displayMessage(Lost_Message);
        }
      }, 10);
    });
  }
  displayBricks(brickWall);
});

/**
 * Create all the bricks of the game.
 */
function createBricks() {
  const bricks = [];
  for (let y = 100, row = 0; row < Bricks_Rows; row++) {
    for (let x = 0, col = 0; col < Bricks_Columns; col++) {
      bricks.push(new Brick(x, y, row + "," + col));
      x += Brick_Width;
    }
    y += Brick_Height;
  }
  return bricks;
}

/**
 * checks if there is a collision between a ball and the paddle.
 * @param {Ball} ball the ball of the game.
 * @param {Paddle} paddle the paddle of the game.
 */
function checkCollisionPaddle(ball, paddle) {
  return (
    $("#balle").offset().top >= $("#raquette").offset().top - Ball_Height &&
    ball.x + Ball_Height > paddle.left &&
    ball.x + Ball_Height < paddle.left + Paddle_Width
  );
}

/**
 * checks if  there is a collision between a ball and any brick.
 * @param {Ball} ball
 * @param {Brick} brick
 */
function checkCollisionBrick(ball, brick) {
  return (
    ball.x - Ball_Width / 2 < brick.x + Brick_Width &&
    ball.x + Ball_Width / 2 > brick.x &&
    ball.y - Ball_Height / 2 < brick.y + Brick_Height &&
    ball.y + Ball_Height / 2 > brick.y
  );
}

/**
 * Return true if the player won , false otherwise.
 * @param {Brick[]} array
 * @param {Player} player
 */
function hasWin(array, player) {
  if (array.length == 0 && player._life > 0) {
    return true;
  } else {
    return false;
  }
}

/**
 * Return true if the player lose , false otherwise.
 * @param {Player} player
 */
function hasLost(player) {
  if (player.life == 0) {
    return true;
  } else {
    return false;
  }
}
