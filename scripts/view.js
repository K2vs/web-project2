"use strict";
/**
 * Display the paddle of the game.
 * @param {Paddle} paddle  of the scene.
 */
function displayPaddle(paddle) {
  $("#raquette").css("left", paddle.left);
}

/**
 * Display the paddle of the game.
 * @param {Ball} ball the of the scene.
 */
function displayBall(ball) {
  $("#balle").css("left", ball._x);
  $("#balle").css("top", ball._y);
  let x = $("#balle").position();
}

/**
 * Display a brick of the game.
 * @param {Brick} brick of the game.
 */
function displayBrick(brick) {
  const div = $("<div>");
  div.prop("id", brick.id);
  div.addClass("brick").css({
    left: `${brick.x}px`,
    top: `${brick.y}px`,
    width: `${Brick_Width}px`,
    height: `${Brick_Height}px`,
  });
  $("#scene").append(div);
}

/**
 * Display all the bricks of the game.
 * @param {Brick[]} array
 */
function displayBricks(array) {
  for (let i = 0; i < array.length; i++) {
    displayBrick(array[i]);
  }
}

/**
 * Delete the brick on the screen.
 * @param {Brick} brick
 */
function removeBrick(brick) {
  document.getElementById(brick.id).outerHTML = "";
}

/**
 *display the score and update it.
 * @param {number} score
 */
function updateScore(score) {
  $("#pointArea").text("your current Score is : " + score + " points");
}

/**
 * display the given message.
 * @param {string} texte the given the message.
 */
function displayMessage(texte) {
  $("#message").show();
  $("#message").text(texte);
}

/**
 * hidden the message.
 */
function hideMessage() {
  $("#message").hide();
}

/**
 * Update the current status of the life of the player.
 * @param {number} life the initial number of life.
 */
function updateLife(life) {
  let nbLives = "";
  for (let i = 0; i < life; i++) {
    nbLives = nbLives + "❤️";
  }
  $("#lifeArea").show();
  $("#lifeArea").text(nbLives);
}

/**
 * display the current level of the player.
 * @param {number} level the current level.
 */
function displayLevel(level) {
  $("#level").show();
  $("#level").text("you are at the level : " + level);
}