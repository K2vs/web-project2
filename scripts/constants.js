const Scene_Height = 600;
const Scene_Width = 960;

const Paddle_Width = 96;
const Paddle_Height = 16;

const Ball_Width = 24;
const Ball_Height = 24;

const Bricks_Columns = 15;
const Bricks_Rows = 5;

const Brick_Width = Scene_Width / Bricks_Columns;
const Brick_Height = 24;

const Brick_Destruction_Point = 25;

const Start_Message = "Click to start";
const Lost_Message = "no more lives ,YOU LOST";
const Win_Message =
  "One more lives , YOU WON , you are moving to the next level  ⭐ ⭐ ⭐";
