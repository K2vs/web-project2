/**
 * Paddle of the game.
 */
class Paddle {
  /**
   * the constructor of the paddle.
   * @param {number} left  the move.
   */
  constructor(left) {
    this._left = this.insideTheScene(left);
  }
  /**
   * the moves of the paddle.
   * @param {number} val the value of the left move.
   */
  moveTo(val) {
    this._left = this.insideTheScene(val);
  }

  /**
   * getter of paddle.
   */
  get left() {
    return this._left;
  }

  /**
   *adjust the left according to the scene.
   * @param {number} left
   */
  insideTheScene(left) {
    if (left < 0) {
      return 0;
    } else if (left > Scene_Width - Paddle_Width) {
      return Scene_Width - Paddle_Width;
      //case the given number is greater than the difference Width between my divs.
    } else {
      return left;
    }
  }
}
