"use strict";
/**
 * The ball of the game.
 */
class Ball {
  /**
   * The constructor of the Ball.
   * @param {number} x
   * @param {number} y
   * @param {number} dx
   * @param {number} dy
   */
  constructor(x, y, dx, dy) {
    this._x = x;
    this._y = y;
    this._dx = dx;
    this._dy = dy;
  }

  /**
   * Get the value of x.
   */
  get x() {
    return this._x;
  }

  /**
   * Get the value of y.
   */
  get y() {
    return this._y;
  }

  /**
   * Get the value of dx.
   */
  get dx() {
    return this._dx;
  }

  /**
   * Get the value of dy.
   */
  get dy() {
    return this._dy;
  }

  /**
   * Moves the ball of the game.
   */
  move() {
    this.hitTheScene();
    this._x = this._dx + this._x;
    this._y = this.dy + this._y;
  }

  /**
   * Reverse the ball when it hits the right or left side.
   */
  _reverseX() {
    this._dx = -this._dx;
  }

  /**
   * Reverse the ball when it hits the top or the bottom.
   */
  _reverseY() {
    this._dy = -this._dy;
  }

  /**
   * Reverse the mouvement of the ball when he hits the borders.
   */
  hitTheScene() {
    if (this.x < 0) {
      this._reverseX();
    } else if (this.x > Scene_Width - Ball_Width) {
      this._reverseX();
    }
    if (this.y < 0) {
      this._reverseY();
    } else if (this._y > Scene_Height - Ball_Height) {
      // this._reverseY();
    }
  }

  /**
   * Reverse the mouvement of the ball when it hits the paddle.
   */
  hitThePadlle() {
    this._reverseY();
  }

  /**
   * Reverse the mouvement of the ball when it hits the paddle.
   */
  hitTheBricks(brick) {
    this._reverseY();
    this._reverseX();
  }

  /**
   * Return true if the ball has touch the floor otherwise return false.
   */
  touchTheFlor() {
    if (this._y > Scene_Height - Ball_Height) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Replace the ball at the middle.
   */
  placeInTheMiddle() {
    this._x = 440;
    this._y = 560;
  }
}
