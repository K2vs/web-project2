"use strict";
/**
 * The bricks of the game.
 */
class Brick {
  /**
   * The constructor of the Bricks.
   * @param {number} x
   * @param {number} y
   * @param {String} id
   */
  constructor(x, y, id) {
    this._x = x;
    this._y = y;
    this._id = id;
  }

  /**
   * Get the value of x.
   */
  get x() {
    return this._x;
  }

  /**
   * Get the value of y.
   */
  get y() {
    return this._y;
  }
  /**
   * Get the value of id.
   */
  get id(){
    return this._id;
  }
}