"use strict";
/**
 * The player of the game.
 */
class Player {
  /**
   * The constructor of the player.
   * @param {number} score the score of the player.
   * @param {number}life the number of heart the player has.
   */
  constructor(score, life) {
    score = 0;
    life = 5;
    this._score = score;
    this._life = life;
  }

  /**
   * Get the value of the score.
   */
  get score() {
    return this._score;
  }

  /**
   * Get the value of the life.
   */
  get life() {
    return this._life;
  }

  /**
   * Add the points to the current score.
   * @param {number} points earned for each breaked brick.
   */
  addToScore(points) {
    this._score = this._score + points;
  }

  /**
   * Remove a life of the player.
   */
  removeLife() {
    this._life = this.life - 1;
  }

  /**
   * Checks if the player still alive.
   */
  stiilAlive() {
    let alive = false;
    while (this._life > 0) {
      alive = true;
    }
    return alive;
  }
  
  /**
   *  Add a life at the player.
   */
  addLife() {
    this._life = this.life + 1;
  }
}
